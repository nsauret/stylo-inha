layout: true
name: layinha

.logo[![logo](media/logo-inha.jpg)]

---

title: Stylo : un éditeur de texte pour les sciences humaines et sociales (INHA)
description: Nicolas Sauret, 12 avril 2021
theme: style.css
class: left, middle, firstslide
template: layinha

# .orange[Stylo] .small[&ndash; un éditeur de texte pour les SHS]

&nbsp;

.sub[
### .orange[Nicolas Sauret]<br/> .small[[HN Lab](https://www.huma-num.fr/hnlab/) (TGIR Huma-Num)<br/> [FabPart Lab](https://fplab.parisnanterre.fr/) (Labex Les passés dans le présent, U. Paris Nanterre)]
]

.footer[
«&nbsp;Les lundis numériques de l'INHA&nbsp;»
&ndash; 12 avril 2021
]


---
class: left, middle,

# .orange[Préambule]

---
class: center, middle,

.large[écrire, éditer]

???

j'ai présenté Stylo dans plusieurs contextes : auprès de chercheurs, auprès d'étudiants, auprès d'éditeurs de revue. Pour ces publics, l'écriture et dans une certaine mesure l'édition sont  deux activités centrales dans leur travail, on pourrait dire que ce sont leurs activités principales : prendre des notes, écrire pour réfléchir, écrire pour enseigner, écrire pour publier, éditer l'écrit, mettre en forme, rendre lisible, rendre visible.

Mais paradoxalement, pour ces praticiens, réfléchir sur leurs pratiques d'écriture et d'édition ne va pas de soi. C'est un effort particulier que de se pencher sur des pratiques profondément inscrites dans l'intellect, on pourrait parler pour ces publics de pratiques expertes, puisqu'après tout c'est le propre du chercheur que d'écrire, que de formaliser et d'exterioriser sa pensée dans un texte ? et le propre de l'éditeur que de créer les conditions de lisibilité d'un texte qu'on lui a soumis ?

Mais il y a un impensé...
---
class: center, middle

.large[traitement de texte]


???

C'est celui des outils d'écriture par lesquels se formalise la pensée.

Tous ces praticiens opérant dans les champs des SHS utilisent dans une très grande majorité un traitement de texte comme Microsoft Word, plus rarement Libreoffice.

Pourquoi tant de Word à l'université ?
Pourquoi la page blanche est elle à ce point incorporé dans les pratiques de tout le corps académique ?
C'est pourtant l'un des pires outils d'écriture et d'édition. Le grand succès d'Apple et de Microsoft a été de démocratiser l'informatique personnelle en invisibilisant les formats, les encodages et les process. L'édition à la portée de tous, sans y penser.

---
class: middle, center

.large[

littératie

réflexivité

]


???

Pour terminer ce préambule, disons que Stylo a été conçu 1. avec l'ambition 1. de tirer les pratiques scripturales et éditoriales des experts de l'écriture et de l'édition vers de meilleures pratiques numériques, c'est-à-dire vers des pratiques qui s'inscrivent réellement dans notre écosystème de savoir contemporain : le web, Internet, le numérique, en tant que fait culturel.

Mais aussi 2. avec l'ambition de faire réfléchir ces experts sur leurs propres pratiques, de permettre une réflexivité qui est elle-même une condition nécessaire pour que l'université, en tant qu'institution, prenne soin de ce qu'on a appelé la chaîne de production de l'écrit.

Dans le contexte de l'INHA, et des institutions culturelles et patrimoniales qui ont pu développer depuis déjà longtemps de bonnes pratiques numériques, dans la gestion de ses collections, dans l'adoption de standard, dans l'encodage de leurs métadonnées, dans leur exposition sur le web, finalement la question de l'écriture numérique telle que je la présente pourra sembler plus évidente. Je l'espère en tout cas et nous pourrons en débattre tout à l'heure.

---
class: left, middle, flexy

.lefty[
## Structure
]
.rigthy[
1. Contexte - génèse et cadre théorique
2. Stylo - présentation et démonstration
3. Stylo en pratique
4. Expérimentation - le dossier «&nbsp;Écrire les communs&nbsp;»
5. Tendances - convergence des écritures
]
???


---
class: left, middle

# .orange[Contexte]

---
class: left, middle

## Cadre institutionnel

Chaire de recherche du Canada sur les écritures numériques

Chaine de production de l'écrit


.flexylogos[
.logos[
![logo-crcen](media/logo-crcen.jpg)
]
.logos[
![logo-crihn](media/logo-crihn.png)
]
.logoserudit[
![logo-erudit](media/logo-erudit.png)
]
.logos[
![logo-hn](media/logo-humanum.png)
]
]

???
Un peu de contexte. Stylo est né dans le giron de la chaire de recherche, engagée dans une réflexion au long court sur la chaine de production de l'écrit, dans différents contextes, dont le contexte universitaire avec l'écriture savante et l'édition savante.

Stylo a bénéficié de différents soutiens financiers :
- celui de la Chaire de recherche du canada sur les écritures numériques, dirigé par Marcello Vitali Rosati. dont l'équipe a conçu et développé l'outil.
- celui du Centre de recherche interuniversitaire sur les humanités numériques, dirigé par Michael Sinatra
- celui d'Erudit, diffuseur des revues de shs au québec et proche voisin et collaborateur de l'université de Montréal. Erudit a vu dans Stylo l'intérêt d'un outil auteur capable de délivrer nativement une édition numérique dans son format de diffusion final, à savoir un format XML au schéma XML Erudit Article.
- et puis Huma-Num a pris le relais une fois les premières versions de Stylo opérationnelles, et a souhaité intégrer Stylo dans son offre de service, nous en reparlerons. Stylo est maintenant hébergée chez Huma-Num, l'équipe de Montréal travaille avec l'équipe du HN Lab, le lab d'HumanNum où je travaille, et la TGIR soutient les développements actuels et à venir de la plateforme.


---
class: left, middle

## Cadre théorique

Nouvelles écritures &rarr; nouvelles pensées


???

On cite souvent Katherine Hayles pour évoquer les effets de l'environnement numérique sur la pensée. Hayles considère en effet que le numérique participe à une mutation épigénétique, une évolution de nos modes de pensée. On retrouve cette hypothèse dans la théorie des supports que l'on connait bien du côté de l'Université de Technologie de Compiègne par exemple  considrant ainsi l'incidence des supports de savoirs sur la pensée elle-même. Ce sont les travaux de Bruno Bachimont, de Bernard Stiegler, eux-mêmes héritant et s'appuyant sur l'anthropologue Jack Goody, sur l'ethnologue André Leroy GOurhan ou encore sur le philosophe Gilbert Simondon.

Mais on retrouve aussi ce tandem entre la technique et la pensée dans les travaux sur les lieux de Savoir de Christian Jacob par exemple, ou encore dans le champ de l'archéologie des médias, c'est-a-dire finalement dans toute réflexion épistémologique sur la fabrique contemporaine des savoirs.

À partir de tout ce bouillon théorique, on peut raisonnablement penser que le numérique comporte tout les ingrédients pour profondément changer le support, l'écriture, et ainsi la pensée. Ce n'est d'ailleurs pas pour rien que ce qu'on envisageait hier comme un fait technique était en fait déjà un fait culturel.


---
class: left, middle

### Pourquoi tant de Word ?

???
Ce nouveau paysage culturel s'est caractérisé par une explosion des modalités de lecture et d'écriture, une explosion des formats culturels et des formes de création.

Et pourtant... un outil s'est imposé dans le milieu universitaire comme l'outil d'écriture par excellence : Microsoft Word

Il ne s'agit pas là de mener bousculer les Gafams, il est assez facile de faire le constat de ses torts, mais comme je le disais aussi, de faire le constat de ses succès. De notre point de vue de chercheurs, c'est plutôt l'université qu'il conviendrait de bousculer.

Stylo est partie d'une grande frustration de voir presque l'entièreté du corps universitaire travailler, écrire, structurer son discours et donc penser avec word.

C'est un peu comme si aujourd'hui les bibliothèques, les musées, les archives, les grandes collections patrimoniales seraient gérées uniquement avec EXCEL. Des tableaux, dont chaque ligne ou chaque cellule générerait des centaines de lignes d'encodage, inexpressives, c'est-à-dire sémantiquement nulle. Une absurdité.

Stylo est parti de ce constat terrible que l'université en tant qu'institution de savoir avait échoué à prendre soin de la chaine de l'écrit. C'est le grand paradoxe. L'institution a peut-être pris soin de l'écrit au sens de sa conservation, mais n'a pas pris soin de l'écrit dans son évolution et dans les innovations qui se jouaient hors de l'institution sur la production de l'écrit.

Alors le constat n'est pas si contrasté heureusement, il suffit de citer la TEI pour considérer que l'université n'est pas complètement défaillante sur la chaine de production de l'écrit. Et hors du projet emblématique de la TEI, il existe bcp d'autres initiatives qui vont dans le bon sens. Mais qui _écrit_ aujourd'hui dans le format TEI ??

---
class: left, middle

### Double dépossession

???
Mais en quoi est-ce si terrible ? Comment cela impacte la publication scientifique et la production de connaissance ? Il s'agit d'une double dépossession.

  - dépossession de l'auteur --> les traitements de texte ne donne aucune maitrise sur la structure de son texte. C'est exactement ce probleme que la TEI a cherché à adresser. N'est-ce pas le chercheur et l'auteur le plus à même de caractériser sémantiquement les diff. éléments de sens de son texte ? D'en identifier les concepts, de les aligner sur des référentiels ? Finalement de produire sur son texte des données scientifiquement pertinentes ?
  - dépossession de l'éditeur --> puisqu'à l'interface avec les diffuseurs numériques, l'éditeur s'est mis lui aussi à éditer sous word, tirant vers le bas la maîtrise et l'expertise que caractérisait sa fonction, la fameuse fonction éditoriale. Word a réduit la littératie que l'éditeur avait sur ses architextes, ce qui se traduit finalement en une perte de pouvoir, voir de légitimité, déléguée aux plateformes de diffusion.

- la solution des plateformes comme OpenEdition Journal engendre plusieurs problèmes :
  - homogénéisation des propositions éditoriales
  - infantilisation des éditeurs ? perte de contrôle, une déprise, un rétrécissement de la fonction de l'éditeur qui est réduit à structurer du word, cad pas grand chose d'un point de vue sémantique.

---
class: left, middle

- Dé-wordiser l'écriture savante
- Redonner à l'auteur et à l'éditeur la maîtrise de la structure et de la sémantique du texte
- Assurer la continuité de la chaîne de données
- Repenser le format de l'article savant

???
C'est le projet de Stylo :

1. dé-wordiser l'écriture savante, l'ancrer dans des formats et des pratiques plus vertueuse et scientifiquement pertinentes
2. Redonner à l'auteur et à l'éditeur la maîtrise de la structure et de la sémantique du texte
3. Assurer la continuité de la chaîne de données: assurer que les enrichissements sémantiques produits par l'auteur soient correctement diffusés
4. Repenser le format de l'article savant

---
class: center, middle
background-image: url(media/source_html_cropped.png)
background-size: contain

### Réapprendre à écrire

.encart[
écrire = écrire + structurer
]

???
Dans cette vision, l’écriture ne se résume plus seulement à écrire. Ecrire dans l'environnement numérique participe d'une dynamique d'édition, autrement dit l'acte d'écriture s'accompagne nécessairement d'un acte d'édition. Cette idée que écrire = écrire + structurer revient à la théorique de l'éditorialisation qui considère que nos écritures s'inscrivent dans un processus de production de l'espace numérique.

Cette culture de l'éditorialisation n'est rien d'autre que la littératie numérique qui ajoute au savoir-lire-et-écrire la maîtrise du milieu d’écriture, une capacité à inscrire l’espace autant qu’à le structurer. Il s'agit de savoir autant évoluer dans cet environnement (l'habiter) que de le faire évoluer (le designer).


---
class: left, middle

### écriture numérique ?

> « Le livre a une réalité physique ou matérielle (le papier, l’encre) et une réalité symbolique ou culturelle (la langue, les signes à interpréter). Mais pour comprendre le fonctionnement du numérique il faut comprendre l’articulation, non pas de deux, mais de trois niveaux : il y a ce qu’écrit la machine, il y a ce qu’écrit le programmeur de cette machine, il y a ce qu’écrit l’utilisateur de cette machine. Lire un document numérique quelconque, c’est lire ces trois niveaux, quoique seul le dernier soit visible.&nbsp;»
>
> .source[&mdash; [Bouchardon et Petit, 2017](http://www.costech.utc.fr/CahiersCOSTECH/spip.php?article69)]

???

Serge Bouchardon et Victor Petit définissent l'écriture numérique comme l'imbrication de trois niveaux décriture numérique :
- ce qu'écrit la machine,
- ce qu'écrit le programmeur de la machine
- ce qu'écrit l'utilisateur de la machine

or deux de ces niveaux ne sont pas visibles, et sont mêmes invisibilisés par les traitements de texte. La proposition de Stylo est de les rendre visibles afin
  1. de rendre possible cette réflexivité sur la pratique d'écriture
  2. de regagner une certaine littératie de l'environnement d'écriture et de savoir.

C'est vraiment recréer le lien entre la sémantique du texte et la sémantique de la machine qui diffuse le texte.


---
class: center, middle, darker, hover
background-image: url(media/styloN_facebook.png)
background-size: contain

.hover[
# .orange[Qu'est-ce que] Stylo .orange[?]
]


???

- un éditeur de texte scientifique implémentant les formats et les bonnes pratiques déjà émergentes

- l'ambition d'institutionnaliser certaines pratiques d'écriture et d'édition pour les communautés SHS.

- conviction que, en tant qu'universitaire, il convenait de prendre autant soin des écrits que l'on étudiaient que de la chaine de production de l'écrit.


---
class: darker
background-image: url(media/stylo_3f.png)
background-size: contain

???
Stylo est à la fois un outil de rédaction de texte scientifique et un outil d'édition de document scientifique.

C'est un outil d'écriture, avec cette vision qu'écrire à l'ère du numérique ne se résume pas à externaliser une pensée dans un discours et un texte, mais nécessite aussi de l'externaliser dans un environnement, c'est à l'éditer, ou plus exactement à l'éditorialiser (selon le concept d'éditorialisation de Vitali-Rosati).

Cet agencement est permis par l'articulation de trois fichiers au coeur de son fonctionnement.

---
class: left, middle, darker
background-image: url(media/stylo_3f_marqued.svg)
background-size: contain

???
- Le corps de texte, au format markdown
- La bibliographie, au format bibtex
- Les métadonnées, au format yaml

On retrouve les principes de Latex : la séparation  du fond et de la forme, même si cela reste relatif dans le cas du Latex. Mais on retrouve aussi le principe d'une gestion bibliographique hors du texte, indépendante de la mise en forme. Il faut savoir que encore aujourd'hui, la plupart des chercheurs, des étudiants mais aussi des éditeurs  passent des heures sur Word à mettre en forme des références bibliographiques pour telle ou telle revue selon des consignes aux auteurs et des conventions typographiques diverses. C'est une pratique qui n'a plus aucun sens aujourd'hui à l'heure du numérique.

---
class: left, middle, darker
background-image: url(media/stylo_fullInterface_2019_cropped_marqued.png)
background-size: contain

???

Je vous montre ici l'interface complète, où l'on retrouve un éditeur de métadonnées, qui vient modifier le fichier yaml associé au document, un gestionnaire de références qui vient modifier le fichier bibtex assoié. Ce gestionnaire est capable de se synchroniser avec une collection zotero en ligne.

On retrouve le corps de texte au centre,

Ainsi que plusieurs modules outillant nos trois sources : ajoutant des fonctionnalités de versionning, la table des matières du corps de texte, des statistiques sur le document et non visible ici un comparateur de version.


---
class: left, middle, darker, inhalist,


### 3 éléments principaux .orange[+] modules techniques

.flexyin[

.lefty[
1. **Éditeur de métadonnées**
  - .orange[yaml]
  - .orange[DC, RDFa, Foaf, ScholarlyArticle, Google Scholar, Prism schema]
  - Vocabulaire contrôlé
  - Alignement avec des autorités (Rameau, Wikidata, LOC) via l'API Isidore (TGIR Huma-Num)
2. **Éditeur de texte** avec balisage interne
  - .orange[markdown enrichi]
  - blocs HTML
3. **Gestion bibliographique**
  - .orange[BibTeX]
  - Api Zotero

]

.righty[
1. **Conversion**
  - .orange[pandoc] (vers html, LateX, pdf)
  - .orange[XSLT] (vers TEI-LOD, Metopes, Érudit)
2. **Versioning**
  - .orange[git]
  - comparateur de versions
3. **Plateforme**
  - MongoDB (bdd)
  - .orange[GraphQL] (backend)
  - ReactJS (frontend)
4. **API**

]]
???
Sans m'y attarder, voici les différents briques logicielles, les différents formats, les différents services qui sont articulés dans Stylo.
Stylo n'invente rien, nous n'avons fait qu'exploiter et mettre ensemble des principes et des outils existants, dans une interface simple, modulaire, agenceant les différents formats et outils.

On a repris le principe de modularité dans les travaux d'Antoine Fauchié : tout est interchangeable : les formats utilisés, les outils qui implémentent ces formats, les logiciels de traitement des formats.

Au fur et à mesure de la conception, nous avons pu remplacer un outil par un autre, nous avons pu améliorer ou optimiser tel ou tel aspect de la chaîne.

Le choix a été de rendre visible certains de ces éléments qui ont été largement délégué aux diffuseurs au détriment de la littératie ces auteurs et des éditeurs.


---
class: left, middle


### text/plain

> The primary subtype of text is "plain".  This indicates plain (unformatted) text.
>
> An interesting characteristic of many such representations is that they are to some extent readable even without the software that interprets them.
>
> .source[source: https://tools.ietf.org/html/rfc1521#section-7.1]


???

- lowtech > pérennité
- mettre à jour les trois niveaux : transparence du format
- favoriser la littératie numérique des auteurs et des éditeurs
- format pivot unique, simple d'accès et d'utilisation, et suffisament riche pour l'édition de textes scientifiques en sciences humaines.


Stylo, c'est le choix du low tech, (et en passant, le choix du logiciel libre, les deux ne sont pas corrélés.)

Ainsi, les trois formats de fichier sont éditables dans stylo bien sûr, mais aussi sur votre ordi ou tout machin numérique, éditable en mode texte brut ou _plain text_. C'est un aspect important car c'est une excellente garantie de pérennité, les sources sont et seront lisibles par toutes les machines. La seule interprétation machinique qui en est fait lors de leur lecture brute est éventuellement une coloration syntaxique permettant de mettre en évidence les syntaxes respectivent du markdown, du yaml et du bibtex.

<!-- Témoigne de la convergence des inscriptions, qu'elle soit programmative ou discursive. Certains ne font d'ailleurs aucune différence entre ces deux inscriptions, tant l'écriture demeure performative. -->

On aurait pu encapsuler ces trois fichiers dans un format .stylo par exemple, comme le font d'autres initiatives similaires. Pourtant, maintenir la transparence sur ces trois fichiers sources nous semblent être davantage porteur de pérennité, de modularité, et de littératie, invitant les usagers à exploiter eux-mêmes les sources, à customiser les exports en jouant avec les paramètres et les filtres de pandoc, ou encore à publier en ligne directement les sources à travers un moteur de sites statiques.

Ainsi, si Stylo facilite et rend plus accessible le process d'édition, de versionning, d'indexation, de conversion vers les formats d'usages, le choix a bien été de conserver un accès direct à la matérialité text/plain des fichiers markdown, bibtex et yaml.

Outre la question de la pérennité des données, ce principe de ne pas masquer les formats et les fichiers va dans le sens d'une technique ouverte, c'est-à-dire visible. Cela va à l'encontre de cette tendance à l'applification qui cherche certe, en adoptant les codes ergonomiques des applications mobiles, à réduire la complexité des interfaces, mais qui n'aboutit en retour qu'à réduire la capacité des utilisateurs.

La transparence du medium porté par Stylo est un premier pas pour la transmission de cette littératie du numérique (savoir-lire-écrire-et-structurer).

---
class: center, middle, darker

.large[# démo !]

---
class: left, middle

# .orange[Stylo en pratique]
---
class: bottom, darker
background-image: url(media/stylo-humanid.png)
background-size: contain

.right[
### Intégration à .orange[HumanID]
]
---
class: top, darker
background-image: url(media/stylo-nouvelles-vues.png)
background-size: contain

### Revue .orange[_Nouvelles vues_]
---
class: top, darker
background-image: url(media/senspublic-home.png)
background-size: contain

### Revue .orange[_Sens public_]

---
class: left, middle, flexy

.lefty[
### _Sens public_
###.orange[Protocole éditorial]
]
.righty[
- soumission des articles et dossiers sur Stylo
- évaluation ouverte ou semi-ouverte
- révision linguistique
- publication au fil de l'eau
]
---
class: left, middle, flexy

.lefty[
### _Sens public_
###.orange[Workflow éditorial]
]
.righty[
- continuité rédaction évaluation édition publication
- dépôt des sources (md, yaml, bib) + formats de diffusion (HTML, PDF, XML) sur un répertoire _git_
  - .arrow[&rarr;] diffusion sur le site de la revue
  - .arrow[&rarr;] diffusion et archivage institutionnel chez le diffuseur Érudit
]

???
continuité de la donnée tout le long de la chaine de production

Première revue qui a été capable de livrer à Erudit un document XML complet : métadonnées + corps de texte (toutes les autres livrant un Word ou pour les revues travaillant des presses universitaires un fichier Indesign)

Pour marquer le fait que l'éditeur et avec lui les auteurs, ont regagné leur capacité à écrire, éditer et structurer les connaissances diffusées.

---
class: left, middle
# .orange[Une expérimentation éditoriale]

---
class: middle, darker
background-image: url(media/senspublic-dossierCommuns2.png)
background-size: contain

---
class: middle, left

- écriture et édition en commun
- ouverture de la gouvernance
- ouverture du protocole
- évaluation ouverte

![](media/communs-phases.png)

???
- une expérimentation : le dossier Ecrire les communs
  - une écriture et une édition en communs
  - ouverture du protocole
  - ouverture de la gouvernance du protocole


---
class: darker
background-image: url(media/communs-sommaire.png)
background-size: contain
---
class: darker
background-image: url(media/communs-lalandeannotation.png)
background-size: contain
---
class: darker
background-image: url(media/communs-lalandeSP.png)
background-size: contain


---
class: left, middle

### Pertinence de Stylo

- pas de protocole à priori
- édition continue
- transparence des conversations : évaluation ouverte

???

  - stylo: pas de protocole ou de processus éditorial à priori: chaque revue met en place son protocole

Stylo a permis :
- une édition continue :
  - cad publier les écrits en cours de production, rendre visible le travail en cours
  - suppose d'être capable de maintenir la continuité des données et du sens, de la rédaction, la formation de la pensée, jusqu'à la diffusion
  - permettre à l'auteur d'intervenir tout au long du processus
- une transparence de l'évaluation

Ce que l'on apprend de cette expérimentation, c'est que les acteurs et les actrices des revues peuvent élaborer leurs propre processus éditorial et se faire ainsi co-concepteur de nouveaux dispositifs et de nouveaux protocoles pour l'appropriation, la circulation, et la  conversation des connaissances.

Il s'agit en quelque sorte de regagner une certaine maîtrise de cette dépossession dont je parlais en début d'intervention, Louise Merzeau parlait de maîtrise de la déprise.

Stylo montre que la fameuse fonction éditoriale peut et doit se renouveller. On a considéré pendant quelques temps que l'écosystème numérique était une machine à désintermédier, alors que l'écosystème était au contraire fortement réintermédié par quelques acteurs silico-vallonnés, qui ont bien saisi eux le changement de paradigme.

Si l'environnement d'écriture et d'édition change aussi radicalement, c'est bien aux éditeurs eux-mêmes de proposer des modèles épistémologiques alternatifs, alternatifs à celui qui dominait ces derniers siècles, mais alternatifs surtout à ceux qui se sont installés ces dernières années.

---
class: left, middle

# .orange[Tendances - convergence des écritures]

???

Je termine rapidement sur quelques éléments de la feuille de route de Stylo, en lien avec une tendance très actuelle de l'écriture scientifique telle qu'on peut l'observer par exemple à Huma-Num à l'écoute des équipes de recherche en SHS.

Cette tendance montre à quel point les pratiques d'écriture scientifique évoluent rapidement. Cette évolution va assez logiquement dans le sens d'une convergence des écritures, caractéristique de cet écosystème des savoirs que constitue le numérique, le web et ce qui lui donne corps : les données

---
class: center, middle,

### observer .arrow[&rarr;] analyser .arrow[&rarr;] écrire et publier

???
De manière très caricatural, dans une ère prénumérique,  les activités de la recherche consistaient à
1. observer (collecter du matériel, collecter des données), analyser (ces données), et sur la base de ces analyses à écrire et à publier des résultats.

Ces activités se déroulaient de manière plus ou moins séquentielle, ou tout du moins dans des espaces d'écriture distincts, peu ou pas compatibles ni interopérables.

---
class: left, middle, flexy

.lefty[
## Un nouveau ménage à trois
]
.righty[
- Données (observer, collecter, agréger)
- Logiciels (agréger, traiter, analyser)
- Publications (écrire, évaluer, valider, publier, diffuser)
]
???

Or l'écosystème de savoir qui s'est élaboré depuis une vingtaine d'année, nous permet désormais de faire tout cela en même temps, dans un même espace, et de le faire de manière collaborative.

On peut ainsi écrire avec les données et avec les résultats des traitements des données, mieux, on peut écrire avec les méthodes de traitements, c'est-à-dire en rendant visible et reproductible les traitements et leurs méthodes.

On peut ainsi raccourcir les cycles d'itération et de validation : on peut montrer la recherche en train de se faire, inviter des pairs dans un processus de validation beaucoup plus vertueux.



---
class: left, middle, flexy

.lefty[
## Calepins

Jupyter Notebook, Jupyter Lab

_écrire, coder, analyser, partager, collaborer, publier_
]
.righty[
![notebook](https://arogozhnikov.github.io/images/jupyter/example-notebook.png)
]

???

Les notebook Jupyter illustrent exactement cette tendance.

Créé en 2015, Jupyter est une plateforme Web qui permet d'écrire des calepins (*notebooks*) contenant à la fois du texte (en markdown) et du code exécutable en Julia, Python, R (voir d'autres langages) ;

Les calepins Jupyter correspondent à cette pratique d'écriture associant la manipulation et le traitement de données, la documentation de ces traitements, et l'analyse des résultats ;

Ils ouvrent à de multiples usages, dont l'édition de de *data-papers* ou d'articles de données, executables. En particulier pour les SHS :  accès à des méthodologies d'analyses en collaborant en direct avec les ingénieurs et permettant la montée en compétence des chercheur•e•s (littératie).

Comme cela fonctionne dans le navigateur (que ce soit en local ou servi par un serveur), les notebooks ont le mérite de réintroduire des pratiques de traitement et d'analyse dans l'environnement du web, cad dans l'environnement principal de production et de circulation du savoir et des connaissances.

---
background-image: url(media/jlab.png)
background-size: contain

???

Le mode Lab permet en effet de gérer plusieurs sources, codes, représentations, et pratiquement de mettre en place des applications et des tableaux de bord sur mesure.

Un espace d'écriture unifié dans le navigateur, un onglet pour le lab: gestion de fichiers, consultation des données, multi-onglets et multi-fenêtre, accès à un terminal, etc.

Besoins
- Écrire des documents (articles, data-papers, etc.) qui exécutent des traitements sur des données (ou à partir de base de données)
- Créer des outils pédagogiques pour traiter des données
- Créer et publier des tableaux de bord pour suivre l'évolution de données

"Discuter autour des données" avec une approche intégrée (on revient à la dimension heuristique de la données, mais aussi à la grande conversation qui caractérise les sciences humaines)

- tester des hypothèses de traitements &rarr; manipulation en temps réel
- discuter et documenter les résultats
- publier la démarche de traitement des données &rarr; *data-papers* adaptés aux principes de la publication académique en SHS

---
class: left, middle, flexy

.lefty[
### .orange[Stylo dans l'écosystème Huma-Num]

de l'article au _data papers_
]
.righty.p40image.center[


![NAKALA](https://www.nakala.fr/build/images/nakala-homepage.png)

# \__Stylo_\_

![Isidore](media/logo-isidore.jpg)

![HumanID](media/logo-humanid.jpg)
]
???
Au sein de l'infrastructure Huma-Num, Stylo a l'ambition de jouer un rôle similaire, en s'interfacant étroitement avec d'un côté NAKALA, comme dépot de données, et ISIDORE, comme agrégateur de contenus scientifiques (publications, dataset, référentiels, etc.).

Il s'agit ainsi d'écrire avec des données, tout en profitant de la richesse sémantique proposée par isidore. En retour, Les contenus issus de Stylo seront susceptibles d'enrichir les données Nakala ainsi que les référentiels Isidore.

---
class: left, middle

# Merci !

Questions/échanges…

.encart[

- Stylo: [stylo.huma-num.fr](https://stylo.huma-num.fr)
- Documentation: [stylo-doc.ecrituresnumeriques.ca](http://stylo-doc.ecrituresnumeriques.ca/)
- Présentation : [nsauret.gitpages.huma-num.fr/stylo-inha](https://nsauret.gitpages.huma-num.fr/stylo-inha/)

]

.footer[
«&nbsp;Les lundis numériques de l'INHA&nbsp;»
&ndash; 12 avril 2021

]
